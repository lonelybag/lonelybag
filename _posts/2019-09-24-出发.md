---
layout:     post
title:      出发
subtitle:   紧张、伤感与激动的交杂
date:       2019-09-24
author:     BY lonelybag
header-img: 
catalog: true
tags:    
      - 出发
marp: false

---

# 流程
## 北京首都机场
- 首先是到相应的区域（在电子屏幕上查找航班号）领登机牌并办理行李托运，可以免费托运2件不超过23kg的行李（超过5kg可能是要支付七百多元），对随身行李的数量和重量没有要求。
<!-- <img src="https://i.loli.net/2019/09/25/zwrHQNlom4SPYMZ.jpg" width = "300" height = "200" style="transform:rotate(90deg);" /> -->
- 进入国际国内分流处的闸门后就要下到2楼乘坐小火车去相应的登机航站楼

[![unmtET.md.jpg](https://s2.ax1x.com/2019/09/26/unmtET.md.jpg)](https://imgchr.com/i/unmtET)

- 然后就是出境的边检、边防和安检
  - 边检是自助进行的，到旁边的台式电脑处，将护照和机票拿出来进行扫描，然后照相
  - 边防时会有一个士兵拿着你的护照对你进行检查
  - 安检时，需要注意的是任何液体都不能携带，此外洗漱包、笔记本电脑和平板电脑都需要拿出来**单独**过安检

[![unmU5F.md.jpg](https://s2.ax1x.com/2019/09/26/unmU5F.md.jpg)](https://imgchr.com/i/unmU5F)
- 去相应的登机口准备登机，这时可以打点水，上个厕所

![unmQ3j.jpg](https://s2.ax1x.com/2019/09/26/unmQ3j.jpg "这个通道处有兑换外币的窗口")
[![unmHVf.md.jpg](https://s2.ax1x.com/2019/09/26/unmHVf.md.jpg)](https://imgchr.com/i/unmHVf)
- 航班起飞前大约20分钟开始登机，乘坐的机型是波音787-9，每排9个座位。

[![unuRcd.md.jpg](https://s2.ax1x.com/2019/09/26/unuRcd.md.jpg)](https://imgchr.com/i/unuRcd)
[![unu21H.md.jpg](https://s2.ax1x.com/2019/09/26/unu21H.md.jpg)](https://imgchr.com/i/unu21H)
- 我的座位号是50D。每个座位都会提供毛毯和一个耳机，座位前的触摸屏幕可以看电影。

[![unu4Bt.md.jpg](https://s2.ax1x.com/2019/09/26/unu4Bt.md.jpg)](https://imgchr.com/i/unu4Bt)
- 大约1点10分进入跑道，比预计的1点05晚了一点。2点会提供一次饮料，但比较诡异的是，2点30分就开始提供第一顿正餐。一共两种，咖喱鸡肉和牛肉面条，我点的咖喱。说实话，那时确实会饿。

[![unKlgH.md.jpg](https://s2.ax1x.com/2019/09/26/unKlgH.md.jpg)](https://imgchr.com/i/unKlgH)

- 吃完饭后飞机的灯光才会暗下来，这时你会发现基本没人睡觉，都在看电影。。。看来晚上的飞机确实容易打乱生活节奏。波音787有个特别的地方，就是窗户能够变色，可以使得机舱的氛围变得适合休息。
- 假设一觉睡到天亮（几乎不可能，中间会多次醒来），就可以上上厕所什么的，早点去一般不用等。

[![unKqIK.md.jpg](https://s2.ax1x.com/2019/09/26/unKqIK.md.jpg)](https://imgchr.com/i/unKqIK "猜猜我在哪。。。")

- 上完厕所后接着睡吧，因为到10点才会提供第二顿也是最后一顿正餐，依然是两种，咖喱鸭肉和红烩牛肉，我这次换成了牛肉。这时机舱也会调亮灯光。

[![unMYQJ.md.jpg](https://s2.ax1x.com/2019/09/26/unMYQJ.md.jpg)](https://imgchr.com/i/unMYQJ)

- 吃完饭就呆着吧。北京时间11点半时，空姐会发入境卡，有中文对照（需要填写的是英文版的，不在图片中），这时随身携带的笔就很有用了，飞机上很多人都在借笔。

[![unQD7q.md.jpg](https://s2.ax1x.com/2019/09/26/unQD7q.md.jpg)](https://imgchr.com/i/unQD7q)

- 下图是距奥克兰还有100km时，大概是奥克兰时间的4点59分。

[![unMcyd.md.jpg](https://s2.ax1x.com/2019/09/26/unMcyd.md.jpg)](https://imgchr.com/i/unMcyd)
- 快到时再上个厕所，这时一般需要排队等位，等位时再照张相

[![unMgOA.md.jpg](https://s2.ax1x.com/2019/09/26/unMgOA.md.jpg)](https://imgchr.com/i/unMgOA)

- 奥克兰的天气不是太好，有小雨，飞机降落的过程中出现了较严重的失速颠簸。。。北京时间12点38降落了。然后下飞机，奥克兰机场感觉不大。

[![unlIMQ.md.jpg](https://s2.ax1x.com/2019/09/26/unlIMQ.md.jpg)](https://imgchr.com/i/unlIMQ)

- 先过边检，可以自助过边检，也就是把护照拿处放在闸机上，然后会识别照片，跟国内一样。然后去取行李，这之前会经过一个购物区，我在那买了一张当地手机卡，vodafone牌的，25 NZD。手机卡调试和支付过程都经历了一些波折。。。

[![un1tyQ.md.jpg](https://s2.ax1x.com/2019/09/26/un1tyQ.md.jpg)](https://imgchr.com/i/un1tyQ)

- 然后取行李，需要看电子指示牌到指定的传送带，这时会有警察领着警犬嗅行李。
- 取完行李后最好休整一下，因为马上就要排队进行入境安检了。也就是检查你的入境卡和随身行李，也是整个旅途最紧张的时刻。
  - 有申报和没申报是不同的队，需要事先看好。安检的队排的非常长，估计等了30分钟才轮到我。先是拿着入境卡和护照到一个工作人员处，他会检查入境卡上的申报项，然后用英语询问你申报的东西是什么，我申报了食物、户外用品和药品。他就问了带了什么食品，是否有蔬菜以及户外运动是在哪进行的，户外用品是什么。药品没有询问。
  - 然后要注意听工作人员的指示，去人工通道还是X光进行下一步安检，我去的X光。跟出境时类似，但是不需要把电脑、平板电脑和洗漱包拿出来。松了很多。
- 这一切都完成后，就算正式踏上新西兰的土地了。大概已经是晚上的6点半了。
- 然后向出口走，我在网上预定了Supershuttle，是一个11人的小车，可以直接送到预定处的地址，每人35 NZD。这个车就在机场出口的马路边上。行李是统一放在车子后面的拖车上的，司机小哥应该是当地毛利人。

[![unJN90.md.jpg](https://s2.ax1x.com/2019/09/26/unJN90.md.jpg)](https://imgchr.com/i/unJN90)

[![unJyNR.md.jpg](https://s2.ax1x.com/2019/09/26/unJyNR.md.jpg)](https://imgchr.com/i/unJyNR "这是小拖车，能看见我的行李吗？")

- 当地时间晚7点半到达目的地了，他是按照距离远近依次送客的，我是第二个

[![unYi80.md.jpg](https://s2.ax1x.com/2019/09/26/unYi80.md.jpg)](https://imgchr.com/i/unYi80)

- 在网上预订的民宿，拿钥匙的流程全程自助。。。下图是房东放在路边栅栏上的密码锁，里面藏着钥匙。。。很适合我

[![unYwGt.md.jpg](https://s2.ax1x.com/2019/09/26/unYwGt.md.jpg)](https://imgchr.com/i/unYwGt)

- 入住后出来吃了点东西，顺便买了点第二天的早饭。晚饭是在华人餐馆吃的香辣土豆肉丝盖饭，12 NZD，可以用微信付款。

[![untdw4.md.jpg](https://s2.ax1x.com/2019/09/26/untdw4.md.jpg)](https://imgchr.com/i/untdw4)

- 早饭是，牛奶和饼干。1 L 的牛奶2NZD，饼干5NZD

[![untNOU.md.jpg](https://s2.ax1x.com/2019/09/26/untNOU.md.jpg)](https://imgchr.com/i/untNOU)
[![untamF.md.jpg](https://s2.ax1x.com/2019/09/26/untamF.md.jpg)](https://imgchr.com/i/untamF)

- 然后简单转了转附近，不得不说，奥克兰的一个特点就是大树，非常粗壮的大树随处可见。整个城市人确实不多，而且都是斜坡，车子开得也很快，还有就是时断时续的雨和大风。。。

[![untIfI.md.jpg](https://s2.ax1x.com/2019/09/26/untIfI.md.jpg)](https://imgchr.com/i/untIfI)

> 一天就这样结束了，也该休息了，但实际上睡不着，因为看了看表，不过才北京时间晚8点而已。。。